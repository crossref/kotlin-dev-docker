FROM alpine:3.13.0

RUN apk add --no-cache openjdk11 && \
    apk add --no-cache curl && \
    apk add --no-cache gradle && \
    apk add --no-cache npm && \
    apk add --no-cache docker && \
    curl -sSLO https://github.com/pinterest/ktlint/releases/download/0.40.0/ktlint && \
    chmod a+x ktlint && \
    mv ktlint /usr/local/bin/
